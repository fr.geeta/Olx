package LogInPage;

import org.openqa.selenium.WebDriver;

public class SignIn {
    private final WebDriver driver;

    public SignIn(WebDriver driver) {
        this.driver = driver;
    }

    public void goToURL(String url) {
        driver.get(url);
    }
}

