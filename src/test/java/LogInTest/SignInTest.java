package LogInTest;

import LogInPage.SignIn;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class SignInTest {
    private static WebDriver driver;

    @BeforeTest
    void init() throws MalformedURLException {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void test() {
        SignIn signIn = PageFactory.initElements(driver, SignIn.class);
        signIn.goToURL("https://www.olx.ua/uk/");

        WebElement logInLink = driver.findElement(By.id("topLoginLink"));
        logInLink.click();

        WebElement userEmail = driver.findElement(By.id("userEmail"));
        userEmail.sendKeys("testusertemplate@gmail.com");

        WebElement userPass = driver.findElement(By.id("userPass"));
        userPass.sendKeys("Testpassword1");

        WebElement rememberMeButton = driver.findElement(By.xpath("//*[@id=\"loginForm\"]/fieldset/div[3]/div/label[1]"));
        rememberMeButton.click();

        WebElement logInButton = driver.findElement(By.id("se_userLogin"));
        logInButton.click();

        WebElement userName = driver.findElement(By.xpath("//*[@id=\"topLoginLink\"]/span/strong"));
        String name = userName.getText();
        Assert.assertEquals(name, "testusertemplate");
    }

    @AfterTest
    public static void quit() {

        driver.quit();
    }
}




